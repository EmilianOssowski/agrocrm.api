﻿using Microsoft.AspNetCore.Identity;

namespace AgroCRM.Models
{
    public class ApplicationUser : IdentityUser
    {
    }

}
